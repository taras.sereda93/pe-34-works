const root = document.body.querySelector("#root");
const timerNodes = document.body.querySelector("#timer");
let intervalCount = 5;

const convertToStr = (arr) => arr.join(' ');

const renderList = (listArray, root) => {
  const generateList = (arr) =>
    arr.map((item) => {
      if (Array.isArray(item)) {
        return `<ul>${convertToStr(generateList(item))}</ul>`;
      } else {
        return `<li>${item}</li>`;
      }
    });
  root.insertAdjacentHTML(
    "beforeend",
    convertToStr(generateList(listArray))
  );
};

const list = [
  "Borispol",
  "Kiev",
  ["Brovary", ["Uman", "Irpin"], "Dnister"],
  "Odessa",
  "Lviv",
  "Chernigiv",
];

renderList(list, root);

const intervalTime = setInterval(() => {
  if (intervalCount === -1) {
    clearInterval(intervalTime);
    root.remove();
    timerNodes.remove();
  } else {
    timerNodes.innerHTML = `${intervalCount}`;
    intervalCount--;
  }
}, 1500);
