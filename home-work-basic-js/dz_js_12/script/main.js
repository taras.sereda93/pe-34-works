const allImage = document.querySelector(".images-wrapper");
const stopButton = document.querySelector(".stop");
const continueButton = document.querySelector(".continue");
let changeTime = 1000;

let current = 0;
const getCycleImg = () => {
  allImage.children[current].hidden = true;
  current++;
  if (current === allImage.children.length) {
    current = 0;
  }
  allImage.children[current].hidden = false;
};

let nextImg = changeTime;

const createCycle = () => {
  return setInterval(() => {
    nextImg -= 4;
    if (nextImg <= 0) {
      nextImg = changeTime;
      getCycleImg();
    }
  });
};

let interval = createCycle();

stopButton.addEventListener("click", () => {
  clearInterval(interval);
});

continueButton.addEventListener("click", () => {
  interval = createCycle();
});
