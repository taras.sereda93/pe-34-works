const button = document.querySelector(".btn");
const inputPassword = document.querySelector(".password_enter");
const inputConfirm = document.querySelector(".password_confirm");
const iconPassword = document.querySelector(".fas.first.fa-eye.icon-password");
const confirmIcon = document.querySelector(".fas.second.fa-eye.icon-password");

const error = document.createElement("span");
error.className = "error";

const errors = {
  empty: "Значення не можуть бути пусті!",
  notEqual: "Потрібно ввести однакові значення!",
};

const isEmpty = (pass) => pass.value.trim() === "";
const comparePasswords = (pass, confirmPass) =>
  pass.value === confirmPass.value;
const putInputMask = (input) => {
  input.type = "password";
  input.classList.replace("fa-eye-slash", "fa-eye");
};
const removeInputMask = (input) => {
  input.type = "text";
  input.classList.replace("fa-eye", "fa-eye-slash");
};
const setError = (errorText) => {
  error.innerText = errorText;
  inputConfirm.after(error);
};
const resetForm = () => {
  error.innerText = "";
  inputPassword.value = "";
  inputConfirm.value = "";
};

iconPassword.addEventListener("click", () => {
  if (inputPassword.type === "password") {
    removeInputMask(inputPassword);
  } else {
    putInputMask(inputPassword);
  }
});

confirmIcon.addEventListener("click", () => {
  if (inputConfirm.type === "password") {
    removeInputMask(inputConfirm);
  } else {
    putInputMask(inputConfirm);
  }
});

button.addEventListener("click", () => {
  let isError = false;

  if (!isError && (isEmpty(inputPassword) || isEmpty(inputConfirm))) {
    isError = true;
    setError(errors.empty);
  }
  if (!isError && !comparePasswords(inputPassword, inputConfirm)) {
    isError = true;
    setError(errors.notEqual);
  }
  if (!isError) {
    alert("You are welcome!");
    resetForm();
  }
});
