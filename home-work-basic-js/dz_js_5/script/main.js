
const generatePassword = (firstName, lastName, birthDay) =>
    firstName[0].toUpperCase() + lastName.toLowerCase() + birthDay.slice(6,10);

function createNewUser() {
    const firstName = prompt('Введіть своє імя.');
    const lastName = prompt('Введіть свою фамілію.');
    const birthDay = prompt('Введіть дату свого народження.', "dd.mm.yyyy");

    const newUsers = {
        firstName,
        lastName,
        birthDay,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getPassword: function() {
            const { firstName, lastName, birthDay } = this;
            return generatePassword(firstName, lastName, birthDay)
        },
        getAge: function(){
            let now = new Date();
            let currentYear = now.getFullYear();
            const [userDay, userMouth, userYear] = this.birthDay.split('.')
            let userBirth = new Date(+userYear, +userMouth-1, +userDay);
            let userBirthInCurrentYear = new Date (now.getFullYear(),userBirth.getMonth(), userBirth.getDate());
            let age = currentYear-(+userYear);
            if(now < userBirthInCurrentYear){
                age = currentYear-(+userYear) - 1;
            }
            return(age)
        }
    }

    console.log (newUsers)
    console.log(`Login : ${newUsers.getLogin()}`)
    console.log(`Age : ${newUsers.getAge()}`)
    console.log(`Password : ${newUsers.getPassword()}`)
}

createNewUser();