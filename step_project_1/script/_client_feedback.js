const slider = document.querySelector(".slider");
const clientFeedbacks = document.querySelectorAll(".feedback_slider_item");
const slaiderImages = document.querySelectorAll(".slider img");

const sliderArrows = document.querySelectorAll(".svg_arrow");
let slideIndex = 0;

slider.addEventListener("click", function (event) {
  const image = event.target;
  const imageAlt = event.target.getAttribute("alt");

  clientFeedbacks.forEach(function (item) {
    const itemAlt = item.dataset.alt;
    if (imageAlt) {
      item.classList.remove("active");

      if (itemAlt === imageAlt) {
        item.classList.add("active");
        slaiderImages.forEach((s) => {
          s.classList.remove("active");
        });
        image.classList.add("active");
      }
    }
  });

  const targetClases = event.target.classList;
  if (targetClases.contains("svg_arrow")) {
    console.log(slideIndex);

    if (targetClases.contains("arrow_prev") && slideIndex > 0) {
      slideIndex--;
      clientFeedbacks.forEach((s) => {
        s.classList.remove("active");
      });
      clientFeedbacks[slideIndex].classList.add("active");
      slaiderImages.forEach((s) => {
        s.classList.remove("active");
      });
      slaiderImages[slideIndex].classList.add("active");
      console.log(event.target);
    }

    if (targetClases.contains("arrow_next") && slideIndex < 3) {
      slideIndex++;
      clientFeedbacks.forEach((s) => {
        s.classList.remove("active");
      });
      clientFeedbacks[slideIndex].classList.add("active");
      slaiderImages.forEach((s) => {
        s.classList.remove("active");
      });
      slaiderImages[slideIndex].classList.add("active");
      console.log(event.target);
    }
  }
});
