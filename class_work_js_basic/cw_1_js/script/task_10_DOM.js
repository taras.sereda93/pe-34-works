
// * Отримайте кілька елементів зі сторінки за допомогою:
// * тег
// * клас
// * ідентифікатор
// * Селектор CSS
// * атрибут імені
// * Використовуйте метод console.dir () для відображення елементів

// let tag = document.getElementsByTagName('h1');

// let geteClassElement = document.getElementsByClassName('title');

// let identifier = document.getElementById('root');

// let selector = document.querySelector('.title');

// let attribute = document.querySelector('[name = "hello"]');

// console.log(tag);
// console.log(tag[0]);
// console.log(geteClassElement);
// console.log(identifier);
// console.log(selector);
// console.log([attribute]);


// * Створіть функцію, яка буде:
// * взяти елемент зі сторінки з класом 'training-list', а текстовий вміст дорівнює 'list-elements 5'.
// * Показати цей елемент у консолі.
// * Замініть текстовий вміст цього елемента на "<p> Привіт </p>", не створюючи новий елемент HTML на сторінці
// * Використовуйте методи масиву для виконання завдання.

// function changeContent() {
//   let ListItems = document.querySelectorAll(".training-list");

//   // console.log(ListItems[0].innerText);
//   // console.log(ListItems[0].textContent);
//   // console.log(ListItems[0].outerText);
//   // console.log(ListItems[0].value);

//   ListItems.forEach((currentItem) => {
//     // console.log(currentItem)
//     if (currentItem.textContent === "list-element 5") {
//       currentItem.innerHTML = "<p> Hello </p>";
//     }
//   });
// }
// changeContent();

// * Отримайте елемент із класом 'remove-me' та видаліть його зі сторінки.
// * Знайдіть елемент із класом "зроби мене більшим". Замініть клас "зроби мене більшим" на "активний". Клас "активний" уже існує в CSS.

// function removeElement() {
//   let elem = document.querySelector(".remove-me");
//   elem.remove();
// }
// removeElement();

// =========================
// (function removeElem() {
//     let elem = document.querySelector('.remove-me');
//     elem.remove()
// })()


// function makeBig() {
//     let elem = document.querySelector('.make-me-bigger')
//     elem.classList.add('active')
//     elem.classList.remove('make-me-bigger')
// }
// makeBig()



// * На екрані користувача є список елементів.
// * Створіть функцію, яка знайде елементи, що закінчилися. Кількість елементів, що закінчилися, дорівнює 0.
// * Замініть 0 у текстовому вмісті цих елементів на "вичерпано" та змініть колір тексту на червоний.

// function changeAmount() {
//   let list = document.querySelectorAll(".storage-item");
//   console.log(list);

//   list.forEach(function (item) {
//     let splitString = item.textContent.split("- ");
//     if (splitString[1] === "0") {
//       item.textContent.replace("0", "run out");
//       item.style.color = "red";
//     }
//   });
// }
// changeAmount();


// * ЗАВДАННЯ - 5
// * Створіть функцію, яка запитуватиме користувача, який саме елемент він хоче змінити.
// * Користувачу потрібно написати лише назву товару. Якщо у списку немає збігів
// * продовжуйте запитувати, поки назва товару не стане дійсним.
// * Потім попросіть нову кількість вибраного товару.
// * Показувати дані, введені користувачем на сторінці.
// * Приклад:
// * назва товару - "кава",
// * нова сума - "17"
// * Це означає, що вам потрібно змінити лише кількість "кави", і зараз вона повинна бути 17.




