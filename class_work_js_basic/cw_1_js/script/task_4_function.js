/* ЗАДАНИЕ - 1
* Написать функцию суммирования.
* Принимает аргументы: первое число и второе число
* Возвращаемое функцией значение: сумма двух аргументов
* */

/* function summ(a,b) {
    return a + b ;
}
console.log(summ(10,15)); */

// EXAMPLE 1
// function showRangeNumbers(start, finish) {
// 	if (finish < start) {
// 		for (let i = finish; i < start; i++) {
// 			console.log(i);
// 		}
// 	} else {
// 		for (let i = start; i < finish; i++) {
// 			console.log(i);
// 		}
// 	}
// }

// EXAMPLE 2
// function showRangeNumbers(start, finish) {
// 	if (finish < start) {
// 		let temp = start;
// 		start = finish;
// 		finish = temp;
// 	}
// 	for (let i = start; i < finish; i++) {
// 		console.log(i);
// 	}
// }


// debugger
// showRangeNumbers(15, 10);
/* 
function filterDataTypes(dataType, ...args) {
  let filterArray = [];

  for (let item of args) {
    if (typeof item === dataType) {
      filterArray.push(item);
    }
  }
  return filterArray;
}

let resultFunc = filterDataTypes(
  "object",
  {},
  null,
  false,
  123,
  "hello",
  32,
  true,
  []
);
console.log(resultFunc);
 */