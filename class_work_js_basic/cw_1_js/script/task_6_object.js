/* ЗАДАНИЕ - 1
* Написать функцию, которая принимает 2 аргумента - имя и возраст пользователя
* Возвращаемое значение этой функции - объект с двумя ключами name и age, куда будут записаны значения переданных функции аргументов.
* */

// function calc(user) {
//   return user;
// }
// calc();

// const user = {
//   name: "Tolik",
//   age: "20",
// };

// console.log(calc(user));


// function main(name, age) {
//   let man = {
//     name,
//     age,
//   };
//   return man;
// }
// main();
// console.log(main("taras", "23"));

/* ЗАДАНИЕ - 2
* Добавить к предыдущему заданию функционал.
* В возвращаемом объекте должен быть метод, который увеличивает возраст на 1.
* Т.е. внутри объекта будет свойство\ключ\поле, значением которого будет являться функция,
* которая увеличивает свойство\ключ\поле age ЭТОГО объекта на 1
* */

// function main(name, age,addAge) {
//   let man = {
//     name: name,
//     age: age,
//     addAge: function () {
//         this.age ++
//     }
 
//   };
//   return man;
// }
// main();
// let result = main("taras", '12');
// result.addAge()
// console.log(result);
  

// function main(name, age,addAge) {
//   let man = {
//     name: name,
//     age: age,
//     addAge: function () {
//         main.age ++
//     },
//     addFiled: function (key,value) {
//         man[key] = value;
//     },
 
//   };
//   return man
// }
// main();
// let result = main("taras", '12');
// result.addFiled('hobbi','master')
// result.addAge()
// console.log(result);

/* Написать функцию которая входным параметром будет принимать обьект,
    возвращаемым значением этой функции должна быть КОПИЯ на этот обьект.
*/

function cloneObject(object) {
    

    return 
}


const obj = {
  name: 123,
};

const obj2 = cloneObject(obj);

console.log(obj2 === obj);

function copy(mainObj) {
  let objCopy = {}; // objCopy будет хранить копию mainObj
  let key;

  for (key in mainObj) {
    objCopy[key] = mainObj[key]; // копирует каждое свойство objCopy
  }
  return objCopy;
}

const mainObj = {
  a: 2,
  b: 5,
  c: {
    x: 7,
    y: 4,
  },
};
const mainObj2 = {
  a: 3,
  b: 6,
  c: {
    x: 8,
    y: 1,
  },
};

console.log(copy(mainObj));
console.log(copy(mainObj2));