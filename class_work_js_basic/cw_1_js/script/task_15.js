
/**
 * Завдання - 01
 *
 * Створіть будь -який текстовий елемент і контейнер з 5 кнопками всередині.
 *
 * Завдання - показати внутрішній текст кнопки, яку щойно натиснули.
 * Можна використовувати лише один слухач подій.
 * */


// const container = document.getElementById('root');
// const text = document.createElement('p');
// container.prepend(text);

// for( let i = 1; i < 6; i++){
//     const btn = document.createElement('button');
//     btn.textContent = `btn-${i}`;
//     btn.id = i;
//     container.append(btn);
// }

// container.addEventListener('click', function(event){
//     if(event.target.tagName === 'BUTTON'){
//     text.textContent = event.target.textContent}

// })

/**
 * Завдання - 02
 *
 * Створіть власне модальне вікно.
 *
 * CSS та розмітка розміщуються у файлах html та css цього класу.
 *
 * Модальне вікно повинно з'явитися після натискання кнопки "Показати модальний".
 *
 * Модальне вікно має закритися після натискання кнопки "хрест" btn у верхньому правому куті або після натискання будь -якого місця на екрані, окрім самого модального.
 * */

// const showModalbtn = document.createElement("button");
// showModalbtn.textContent = "Открить модальне вікно";
// document.body.prepend(showModalbtn);

// showModalbtn.addEventListener("click", () => {
//   const modalWrapper = document.querySelector(".modal-wrapper");
//   modalWrapper.style.display = "flex";
//   modalWrapper.addEventListener("click", function (event) {
//     if (
//       event.target === event.currentTarget ||
//       event.target.classList.contains("modal-close")
//     ) {
//       modalWrapper.style.display = "none";
//     }
//   });
// });


//=========================

// const showModalBtn = document.createElement("button");
// showModalBtn.textContent = "Открыть модальное окно";
// document.body.prepend(showModalBtn);

// function createModal(text) {
//   const modal = document.createElement("div");
//   modal.className = "modal-wrapper";

//   modal.insertAdjacentHTML(
//     "afterbegin",
//     `<div class="modal">
//             <button class="modal-close">x</button>
//             <p class="modal-text">${text}</p>
//         </div>`
//   );

//   modal.addEventListener("click", function (ev) {
//     if (
//       ev.target === ev.currentTarget ||
//       ev.target.classList.contains("modal-close")
//     ) {
//       modal.remove();
//     }
//   });

//   return modal;
// }

// showModalBtn.addEventListener("click", () => {
//   const modalInBody = createModal(
//     "Lorem ipsum dolor sit amet, consectetur adipisicing elit"
//   );

//   document.body.prepend(modalInBody);
// });








