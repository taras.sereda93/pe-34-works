//=========================

// const array = ["hello", "i", "student", "danit"];

// let index = 0;
// const showList = setInterval(() => {
//   if (index === array.length) {
//     return;
//     // clearInterval(showList)
//   } else {
//     console.log(array[index]);
//     index++;
//   }
// }, 500);

//=========================

/* ЗАВДАННЯ - 1
* Написати функцію showMsg (msgText, time), де:
* msgText - текст повідомлення, яке буде показано
* час - кількість мілісекунд затримки для відображення повідомлення
* */

// function showMsg(msgText, time) {
//   setTimeout(() => {
//     console.log(msgText);
//   }, time);
// }
// showMsg("hello func it`s work", 1500);

//=========================

// function showMsg(msgText, time, count) {
//   const timer = setInterval(() => {
//     console.log(msgText);
//     count--;
//     if (count <= 0) {
//       clearInterval(timer);
//     }
//   }, time);
// }

// showMsg('hello func it"s work', 1000, 4);

//=========================

//========================= LOCAL STORAGE ===============

// localStorage.setItem("theme", "dark");

// const get = localStorage.getItem("theme");
// console.log(get);

//=========================


/* ЗАВДАННЯ - 3
* Перевірте, чи має localStorage якесь значення, підключене до ключа "userName".
* Якщо localStorage містить значення за ключем "userName", покажіть повідомлення "Hello, userName", розмістіть фактичне значення зі сховища замість userName.
* Якщо в localStorage немає ключового userName:
* - попросіть користувача ввести своє ім'я.
* - збережіть його в localStorage за допомогою ключа userName
* - показати повідомлення Hello, userName
* */

const localStorageUserName = localStorage.getItem('userName');

if(localStorageUserName !== null){
    console.log(`Hello ${localStorageUserName}`);


}
else{
    const userName = prompt('Enter your nsername');
    localStorage.setItem('userName', userName)
    console.log(`Hello ${userName}`);
}







