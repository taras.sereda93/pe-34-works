
/* Показувати сповіщення з повідомленням "Це натискання" 
після натискання кнопки "Натисніть мене". */


/* const button = document.createElement('button')
button.textContent = 'Click me'
document.body.insertAdjacentHTML('beforeend', button)

button.addEventListener('click', function (){
    alert('This is click')
}) */


/* * Показувати сповіщення з повідомленням "Це наведення курсору"
 після наведення курсора на кнопку "Натисніть мене". */

/*  const button = document.createElement('button')
 document.body.prepend(button)
 button.textContent =  'Click me'
 button.onmouseover = function () {
     alert("Це наведення курсору")
 }
 */

/*ЗАВДАННЯ - 3
* Створіть функцію, яка буде випадковим чином змінювати колір фону квадрата 100px,
* натиснувши на неї. Кожен колір має бути випадковим, прозорим і білий не входить до списку кольорів.*/

/* function createSquare(size) {
  const block = document.createElement("div");
  block.style.width = size + "px";
  block.style.height = size + "px";
  block.style.backgroundColor = "#000";
  return block;
}

const randomColor = (params) => Math.floor(Math.random() * params);

const square = createSquare(100);
document.body.prepend(square);
square.addEventListener("click", function () {
  let changeColor = `rgb(${randomColor(255)},${randomColor(55)},${randomColor(
    155
  )})`;
  this.style.backgroundColor = changeColor;
});
 */


/*ЗАВДАННЯ - 4
* Біля квадрата 100px є кнопка "Змінити колір". 
Створіть функцію, яка буде змінювати колір квадрата 
випадковим чином, натиснувши кнопку, а не квадрат.
*/

/* function createSquare(size) {
  const block = document.createElement("div");
  block.style.width = size + "px";
  block.style.height = size + "px";
  block.style.backgroundColor = "#000";
  return block;
}
const square = createSquare(100);
document.body.prepend(square);

const randomColor = (params) => Math.floor(Math.random() * params);

function createButton(callback) {
  const btn = document.createElement("button");
  btn.innerText = "Змінити колір на інший";
  btn.addEventListener("click", callback);
  return btn;
}

const button = createButton(changeColor);
square.after(button);

function changeColor() {
  let changeColor = `rgb(${randomColor(255)},${randomColor(55)},${randomColor(
    155
  )})`;
  square.style.backgroundColor = changeColor;
} */

/*ЗАВДАННЯ - 5
* Біля квадрата 100px є введення.
* У цей вхід можна ввести тільки колір HEX кольору. 
* Поряд із цим входом - btn "Ok" розміщено, і він за замовчуванням неактивний.
* Створіть функцію, яка буде змінювати колір.
* Після введення шістнадцяткової кнопки "Ok" повинна стати активною, щоб користувач міг натиснути на неї.
* Після натискання кнопки "Ok" колір повинен змінитися.
* */

/* function createSquare(size) {
	const block = document.createElement('div')
	block.style.width = size + 'px'
	block.style.height = size + 'px'
	block.style.backgroundColor = '#000'
	return block
}
const square = createSquare(100)
document.body.prepend(square)

const inp = document.createElement('input')
inp.type = 'color'
document.body.append(inp)

const btn = document.createElement('button')
btn.textContent = 'Ok'
document.body.append(btn)

btn.addEventListener('click', () => {
	square.style.backgroundColor = inp.value
}) */



