/*** ЗАВДАННЯ ***
 * 1) Відображати всі електронні листи на екрані, використовуючи лише елементи створення JS.
 * Кожен лист повинен мати прихований текст. Показувати текст лише після того, як користувач натисне на елемент електронної пошти.
 * Необхідно використовувати лише один прослуховувач подій для контейнера з літерами.
 *
 * 2) Реалізуйте текстовий ефект перемикання. Це означає, що одночасно може відображатися лише один текст лише на букві.
 * Якщо користувач натиснув літеру, у якій немає тексту - вам потрібно закрити поточний відкритий текст, а потім відкрити текст для листа, на який щойно натиснули.
 *
 * 3) Створіть кнопку "Нова пошта". Після натискання цієї кнопки користувачеві потрібно побачити модальне вікно з формою для створення листу електронної пошти.
 * Поля для цієї форми: Назва електронної пошти, Кому (електронна адреса одержувача), текст листа (до 500 символів), кнопка "Надіслати". До речі, вам потрібно автоматично заповнити властивість 'from' для кожного листа, і це буде "gogi@gogimail.go".
 * Якщо користувач хоче закрити модальне вікно, він може це зробити, натиснувши на знак хреста у верхньому правому куті модального вікна.
 * Модальний розмір вікна - 500 пікселів як по ширині, так і по висоті. Він відображається у нижньому правому куті сторінки, користувач може отримати доступ до будь -якої іншої частини функціональності, поки відображається модальне вікно.
 *
 * 4) Кожен лист має мати кнопку "Видалити", яка видалить цей лист як зі сторінки, так і з mailStorage.
 * */

// There is some `mailStorage` - a little simulation of real storage with email letters.

const mailStorage = [
  {
    subject: "Hello world",
    from: "gogidoe@somemail.nothing",
    to: "lolabola@ui.ux",
    text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
  },
  {
    subject: "How could you?!",
    from: "ladyboss@somemail.nothing",
    to: "ingeneer@nomail.here",
    text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
  },
  {
    subject: "Acces denied",
    from: "info@cornhub.com",
    to: "gogidoe@somemail.nothing",
    text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
  },
];

const parent = document.querySelector(".emails");

function showEmails(data) {
  data.forEach((item) => {
    const wrapper = document.createElement("div");
    wrapper.className = "email-item";
    wrapper.insertAdjacentHTML(
      "afterbegin",
      `<h4 class="email-subject">${item.subject}</h4>

        <div class="email-subtext-wrapper">
            <a href="mailto:${item.from}" class="email-subtext email-from">${item.from}</a>
            <a href="mailto:${item.to}" class="email-subtext email-to">${item.to}</a>
        </div>

        <p class="email-text" hidden>${item.text}</p>`
    );
    wrapper.addEventListener("click", function (ev) {
      // console.log(ev.currentTarget.querySelector('.email-text'));

      const text = ev.currentTarget.querySelector(".email-text");

      text.hidden = !text.hidden;
    });

    parent.append(wrapper);
  });
}
showEmails(mailStorage);








// const parent = document.querySelector('.emails')

// parent.addEventListener('click', function(ev) {
//   console.log(ev.target.closest('.email-item'))
//   const text = ev.target.closest('.email-item').querySelector('.email-text')
//   text.hidden = !text.hidden
// })


// function showEmails(data) {
// 	data.forEach(item => {
// 		const wrapper = document.createElement('div')
// 		wrapper.className = 'email-item'

// 		wrapper.insertAdjacentHTML(
// 			'afterbegin',
// 			`<h4 class="email-subject">${item.subject}</h4>
//         <div class="email-subtext-wrapper">
//             <a href="mailto: ${item.from}" class="email-subtext email-from">${item.from}</a>
//             <a href="mailto: ${item.to}" class="email-subtext email-to">${item.to}</a>
//         </div>
//         <p class="email-text" hidden>${item.text}</p>
//     `,
// 		)

// 		// let checked = true
// 		// wrapper.addEventListener('click', function (ev) {
// 			// console.log(ev.target.closest('.email-item').querySelector('.email-text'));
// 			// console.log(ev.currentTarget.querySelector('.email-text'));
// 			// const text = ev.currentTarget.querySelector('.email-text')

// 			// if (checked) {
// 			// 	text.hidden = false
//       //   checked = true
// 			// } else {
//       //   text.hidden = true
//       //   checked = false
// 			// }

// 			// text.hidden = !text.hidden
// 		// })

// 		parent.append(wrapper)
// 	})
// }

// showEmails(mailStorage)