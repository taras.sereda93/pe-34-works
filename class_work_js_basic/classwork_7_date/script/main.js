/**
 * Задание 2.
 *
 * Написать функцию, capitalizeAndDoublify, которая переводит
 * символы строки в верхний регистр и дублирует каждый её символ.
 *
 * Условия:
 * - Использовать встроенную функцию repeat;
 * - Использовать встроенную функцию toUpperCase;
 * - Использовать цикл for...of.
 */
/* Решение */
/* Пример */
// console.log(capitalizeAndDoublify('hello')); // HHEELLOO
// console.log(capitalizeAndDoublify('JavaScript!')); // JJAAVVAASSCCRRIIPPTT!!

// function capitalizeAndDoublify(str) {
//     str = str.toUpperCase();
//     let result = '';
//     for (let char of str){
//         result = result + char.repeat(2);
//     }
//     return result;
// }

// console.log(capitalizeAndDoublify('hello'));
// console.log(capitalizeAndDoublify('JavaScript!'));

/**
 * Задание 1.
 *
 * Написать имплементацию встроенной функции строки repeat(times).
 *
 * Функция должна обладать двумя параметрами:
 * - Целевая строка для повторения;
 * - Количество повторений целевой строки.
 *
 * Функция должна возвращать преобразованную строку.
 *
 * Условия:
 * - Генерировать ошибку, если первый параметр не является строкой,
 * а второй не является числом.
 */
/* Решение */
/* Пример */

function repeat(str, qty) {
    if (typeof str !== 'string') {
        throw Error ('Error: First parameter should be a string type.')
    }
    if (isNaN(qty)){
        throw Error ('Error: First parameter should be a number type.')
    }
    let a = ''
    for (let i=0; i<= qty; i++ ){
        a = a + str
    }
    return a


    
}



const string = 'Hello, world!';
console.log(repeat(string, 3)); // Hello, world!Hello, world!Hello, world!
console.log(repeat(string, 1)); // Hello, world!
console.log(repeat(string, 2)); // Hello, world!Hello, world!
console.log(repeat(string, 5)); // Hello, world!Hello, world!Hello, world!Hello, world!Hello, world!
console.log(repeat(7, 5));      // Error: First parameter should be a string type.

