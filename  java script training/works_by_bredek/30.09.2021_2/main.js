const questionBtns = document.querySelectorAll(".question-btn");
const question = document.querySelectorAll(".question");

questionBtns.forEach(function (item) {
  item.addEventListener("click", function (event) {
    const targetQuestion = event.currentTarget.parentElement.parentElement;

    question.forEach(function (ok) {
      if (ok !== targetQuestion) {
        ok.classList.remove("show-text");
      }
    });

    targetQuestion.classList.toggle("show-text");
  });
});
















