const btn = document.querySelector(".btn");
const modal = document.querySelector(".modal");
const box = document.querySelector(".box");
const cloused = document.querySelector(".cloused");

btn.addEventListener("click", function () {
  toggleModal();
  /*   modal.classList.add("modal_opened");
  console.log("click"); */
});

box.addEventListener("click", function (event) {
  check(event.target);

  console.log(event.target.tagName);
});

function check(target) {
  if (target.tagName === "LI" && target.innerHTML === "Hello world 2") {
    toggleModal();
    alert(target.innerHTML);
    /*     modal.classList.remove("modal_opened"); */
  }
}

function toggleModal() {
  modal.classList.toggle("modal_opened");
}

cloused.addEventListener("click", function () {
  toggleModal();
});
