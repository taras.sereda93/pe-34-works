const btn = document.querySelector(".btn");
const color = document.querySelector(".color");
const randomColor = (params) => Math.floor(Math.random() * params);

function changeColor() {
  let changeColor = `rgb(${randomColor(255)},${randomColor(55)},${randomColor(
    155
  )}
  )`;
  color.innerHTML = changeColor;
  document.body.style.backgroundColor = changeColor;
  btn.style.backgroundColor = changeColor;
}

btn.addEventListener("click", function () {
  changeColor();

});
