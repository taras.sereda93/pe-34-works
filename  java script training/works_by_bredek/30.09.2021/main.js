const getData = async () => {
  const res = await fetch(
    "https://www.thecocktaildb.com/api/json/v1/1/search.php?s="
  );

  const data = await res.json();

  const sectionCenter = document.getElementById("section-center");

  const drinks = data.drinks
    .map(function (item) {
      return `<a href="./drink.html" >
    <article class="cocktail" >
    <img src="${item.strDrinkThumb}" alt="${item.strDrink}" />
    <h3>${item.strDrink}</h3>
    </article>
    </a>`;
    })
    .join("");

  sectionCenter.innerHTML = drinks;
};

getData();
