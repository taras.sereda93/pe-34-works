const value = document.getElementById("value");
const btnDecreases = document.querySelector(".decrease");
const btnResets = document.querySelector(".reset");
const btnIncreases = document.querySelector(".increase");

let counter = 0;

const buttons = document.querySelectorAll(".btn");

buttons.forEach(function (btn) {
  btn.addEventListener("click", function (event) {
    const btnClasess = event.target.classList;
    setCounter(btnClasess);
    setColor(counter);

    value.textContent = counter;
  });
});

function setCounter(btnClasess) {
  if (btnClasess.contains("decrease")) {
    counter--;
  }
  if (btnClasess.contains("increase")) {
    counter++;
  }
  if (btnClasess.contains("reset")) {
    counter = 0;
  }
}

function setColor(counter) {
  if (counter > 0) {
    value.style.color = "green";
  }
  if (counter < 0) {
    value.style.color = "red";
  }
  if (counter === 0) {
    value.style.color = "black";
  }
}

/* btnIncreases.addEventListener("click", function () {
  counter++;
  value.textContent = counter;
  if (counter > 0) {
    value.style.color = "green";
  } else if (counter === 0) {
    value.style.color = "black";
  }
  //console.log(counter);
});

btnDecreases.addEventListener("click", function () {
  counter--;
  value.textContent = counter;
  if (counter < 0) {
    value.style.color = "red";
  } else if (counter === 0) {
    value.style.color = "black";
  }
  // console.log(counter);
});

btnResets.addEventListener("click", function () {
  counter = 0;
  value.textContent = counter;
  if (counter === 0) {
    value.style.color = "black";
  }
  // console.log(counter);
});
 */
