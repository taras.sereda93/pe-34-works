// Прості типи данних

// const myNumber = 2525;
// const myString = 'String Some';
// const myBoolean = true;
// const myNull = null;
// const myUndef = undefined;

// console.log(typeof myNumber);
// console.log(typeof myString);
// console.log(typeof myBoolean);
// console.log(typeof myNull);
// console.log(typeof myUndef);

// Обєктні типи данних

// console.log("");

// const object = { name:'Jora'},
// array =[1,2,3],
// regexp = /w+/g,
// func = function () {};

// console.log(typeof object);
// console.log(typeof array);
// console.log(typeof regexp);
// console.log(typeof func );

// object.name = 'Tolik';
// array[0] = 200000;
// console.log(array );

/* let a ;

for ( a = 2 ; a <= 100 ; a++ ) {
    if ( a % 2 === 0) { 

        console.log(a);
    }

}; */



/* let str ='Somtimes the same is different';

console.log(str.length); // виводить довжину строки
console.log(str.charAt(str.length - 1)); // виводить крайню букву строки
console.log(str.substring(9)); // виводить строку з вказаого місця\
console.log(str.substring(9,21)); // виводить від і до
console.log(str.slice(-10)); // вирізає 10 останніх символів
console.log(str.substr(13,4)); // від начально заданого місці і до якого в виді кількості символів
console.log(str.indexOf('me')); // з якого місця починати рахувати строчку чи з якого слова

str = str.replace('is','is not');
console.log(str); // змінює сам текст в строці або елемент тексту(його частину)

console.log(str.split(' ')); // розбивка нашої строки на масив

console.log(str.toUpperCase()); // строка великими буквами 
console.log(str.toLocaleLowerCase()); // строка маленькими буквами
console.log( str[4]); // використовуєтся замість методу charAt для того щоб повернути символ з опредільонним індексом
 */


/* 
function isEmpty(string) {
	if(string.length === 0) {
  	console.log("string is blank")
  } else {
  	console.log("string is not blank")
  }
}

isEmpty('Hello!');
isEmpty("");
 */


// Задача 1 Задача. Создайте переменную str и присвойте ей значение 'abcde'. 
// Обращаясь к отдельным символам этой строки выведите на экран символ 'a', символ 'b', символ 'e'.
/* let str = 'abcde'
console.log(str[0]);
console.log(str[1]);
console.log(str[2]);
console.log(str[3]);
console.log(str[4]); */

// Задача 2 Напишите скрипт, который считает количество секунд в часе.
/* å */

//Задача 3 Переделайте приведенный код так, чтобы в нем использовались операции +=, -=, *=, /=, ++, --. 
// Количество строк кода при этом не должно измениться. Код для переделки:
/* var num = 1;
num = num += 12;
num = num -= 14;
num = num *= 5;
num = num /= 7;
num = num ++ ;
num = num -- ;
alert(num); */







// Задачи по числам

// Задача 1 Создайте переменную num и присвойте ей значение 3. Выведите значение этой переменной на экран с помощью метода alert.
/* let num = 3;
alert(num); */

// Задача 2 Создайте переменные a=10 и b=2. Выведите на экран их сумму, разность, произведение и частное (результат деления).
/* 
let a = 10;
let b = 2;
let c = a - b;
console.log(c); */

// Задача 3 Создайте переменные c=15 и d=2. Просуммируйте их, а результат присвойте переменной result. Выведите на экран значение переменной result.
/* let c = 15;
let d = 2;
let result = c + d;
console.log(result); */

// Задача 4 Создайте переменные a=10, b=2 и c=5. Выведите на экран их сумму.

/* let a = 10;
let b = 2;
let c = 5;
let result = a + b + c;

console.log(result); */

// Задача 5  Создайте переменные a=17 и b=10. Отнимите от a переменную b и результат присвойте переменной c. Затем создайте переменную d, присвойте ей значение 7. Сложите переменные c и d, а результат запишите в переменную result. Выведите на экран значение переменной result.
/* let a = 17;
let b = 10;
let c = a - b;
let d = 7;
let result = d + c;
console.log(result); */

// Задачи по строкам

// задача 1  Создайте переменную str и присвойте ей значение 'Привет, Мир!'. Выведите значение этой переменной на экран.

/* let str = 'Hello World';
alert(str); */

// Задача 2 Создайте переменные str1='Привет, ' и str2='Мир!'. С помощью этих переменных и операции сложения строк выведите на экран фразу 'Привет, Мир!'.

/* let str1 = 'Hello ';
let str2 = 'World';
let result = str1 + str2; 
alert(result); */

// Задача 3 Создайте переменную name и присвойте ей ваше имя. Выведите на экран фразу 'Привет, %Имя%!'.

/* let name1 = 'Taras';
console.log('Hello, ' + name1 + '!'); */

// Задача 4  Создайте переменную age и присвойте ей ваш возраст. Выведите на экран 'Мне %Возраст% лет!'.

/* let age = '28';
console.log('Мені ' + age + ' років !'); */

// Функція Promt

// Задача 1 Спросите имя пользователя с помощью методы prompt. Выведите с помощью alert сообщение 'Ваше имя %имя%'.
/* let name1 =prompt('Введіть ваше імя!');
alert('Ваше імя, ' + name1); */

// Задача 2 Спросите у пользователя число. Выведите с помощью alert квадрат этого числа.
/* let nam =prompt('Введіть число !');
let result = nam * nam;
alert(result); */

// Звернення до символів строки

// Задача 1  Создайте переменную str и присвойте ей значение 'abcde'. Обращаясь к отдельным символам этой строки выведите на экран символ 'a', символ 'c', символ 'e'.
/* let str = 'abcde';
console.log(str[0],str[2],str[4]); */

// Задача 2 Создайте переменную num и присвойте ей значение '12345'. Найдите произведение (умножение) цифр этого числа.
/* const str = "12345";
const splitedStr = str.split("");
console.log(splitedStr);
let count = 0;
const result = splitedStr.forEach(item => {
 count += +item;
})
console.log(count); */

// Практичні завдання 

// Задача 1 Напишите скрипт, который считает количество секунд в часе, в сутках, в месяце.
/* let hour = 60 * 60;
console.log(hour);
let day = 60 * 60 * 24;
console.log(day);
let month = 60 * 60 * 24 * 30;
console.log(month); */

/* const secondsInMinute = 60;
const secondsInHour = secondsInMinute * 60;
const secondsInDay = secondsInHour * 24;
const secondsInMonth = secondsInDay * 30;
console.log({secondsInMinute, secondsInHour, secondsInDay, secondsInMonth});
 */

// Задача 2 Создайте три переменные - час, минута, секунда. С их помощью выведите текущее время в формате 'час:минута:секунда'.
/* let hour ='12';
let minut ='08';
let second = '36';
console.log(hour + ":" + minut + ":" + second); */

// Задача 3 Создайте переменную, присвойте ей число. Возведите это число в квадрат. Выведите его на экран.
/* let nam =prompt('Введіть число !');
let result = nam * nam;
alert(result);  */

// Робота з присвоєнням і директивами 

// Задача 1 Переделайте этот код так, чтобы в нем использовались операции +=, -=, *=, /=. Количество строк кода при этом не должно измениться.

/* var num = 47;
num +=  7;
num -=  18;
num *=  10;
num /=  15;
alert(num); */

// Задача 2 Переделайте этот код так, чтобы в нем использовались операции ++ и --. Количество строк кода при этом не должно измениться.

/* var num = 10;
num ++;
num ++;
num  --;
alert(num);
 */







// 









































