// Задачи на конструкции if-else, switch-case в JavaScript

// Работа с if-else
// 1.
/* const a = 3;
if (a == 3){
    alert(
        'Верно'
    )
}
else {
    alert(
    'Неверно'
    )
}; */

// 2.
/* const a = 1;
if (a > 0){
    alert(
        'Верно'
    )
}
else {
    alert(
    'Неверно'
    )
};
 */

// 3.
/* const a = 1;
if (a < 0){
    alert(
        'Верно'
    )
}
else {
    alert(
    'Неверно'
    )
}; */

// 4.

/* const a = 10;
if (a >= 9){
    alert(
        'Верно'
    )
}
else {
    alert(
    'Неверно'
    )
};  */

// 5.

/* const a = 10;
if (a <= 11){
    alert(
        'Верно'
    )
}
else {
    alert(
    'Неверно'
    )
};  */

// 6.

/* const a = 10;
if (a != 12){
    alert(
        'Верно'
    )
}
else {
    alert(
    'Неверно'
    )
};  
 */
// 7.
/* const a = 'test';
if (a == 'test'){
    alert(
        'Верно'
    )
}
else {
    alert(
    'Неверно'
    )
};   */

// 8.
/* const a = '1';

if (a === '1'){
    alert(
        'Верно'
    )
}
else {
    alert(
    'Неверно'
    )
};    */

// 9.
/* const test = true;
if(!test){
    alert(
        'Верно'
    )
}
else {
    alert(
    'Неверно'
    )
};  */

// Работа с && (и) и || (или)
// 10.
/* const a = 3;
if( a > 0 && a < 5){
    alert(
        'Верно'
    )
}
else {
    alert(
    'Неверно'
    )
};
 */
// 11.

/* const a = 5;
if (a === 0 || a === 2){
    const b = a + 8;
    console.log(b);
}
else{
    const c = a / 5;
    console.log(c);
}
 */

12.
/* const a = 1;
const b = 3;
if( a <= 1 || b >= 3 ){
    c = a + b;
    console.log(c);
}
else{
    const d = a - b;
    console.log(b);
} */

13.
/* const a = 10;
const b = 30;
if( a > 2 && a < 11 || b >= 6 && b < 14 ){
    alert(
        'Верно'
    )
}
else {
    alert(
    'Неверно'
    )
};  
 */


14.
/* let num = 2;
switch (num) {
  case 1:
    console.log("Зима");
    break;
  case 2:
    console.log("Весна");
    break;
  case 3:
    console.log("Літо");
    break;
  case 4:
    console.log("Осінь");
    break;

  default:
    console.log("Пятого нажаль неісснує");
    break;
} */

15.
/* const day = 23;
if( day > 0 && day < 10){
    console.log(" 1 decade");
} else if( day > 10 && day < 20 ){
    console.log( " 2 decade");
} else if( day >21 && day < 31){
    console.log(" 3 decade");
}; */

16.

/* const a = 6;
switch (a) {
  case 1:
  case 2:
  case 12:
    console.log("zima");
    break;
  case 3:
  case 4:
  case 5:
    console.log("vesna");
    break;
  case 6:
  case 7:
  case 8:
    console.log("lito");
    break;
  case 9:
  case 10:
  case 11:
    console.log("osin");
    break;

  default:
    break;
}; */

18.

/* const a = "123";
const splitedString = a.split("");

const firstNumber = +splitedString[0];
const secondNumber = +splitedString[1];
const thirtNumber = +splitedString[2];
const result = firstNumber + secondNumber + thirtNumber;

console.log(result); */

19.

/* const a = "123456";
const splitedString = a.split("");

const firstNumber = +splitedString[0];
const secondNumber = +splitedString[1];
const thirtNumber = +splitedString[2];
const fourNumber = +splitedString[3];
const fivetNumber = +splitedString[4];
const sixNumber = +splitedString[5];

const result = firstNumber + secondNumber + thirtNumber;
const result2 = fourNumber + fivetNumber + sixNumber;

if( result === result2){
    console.log('verno');
} else if(result != result2){
    console.log('ne verno');
};
 */
























